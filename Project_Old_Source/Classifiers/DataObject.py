import csv
import numpy as np

class BaseDataContainer:
    def __init__(self,vector_of_rows):
        self.base_data = vector_of_rows
        self.rows = len(self.base_data)

        self.complete_set = self.getDataSubset(self.getFullDataIndex());

    def getDataSubset(self,vector_of_indexes):
        returnValue = []

        for i in vector_of_indexes:
            returnValue.append(np.array(self.base_data[i]))

        return np.array(returnValue)

    def getDataCount(self):
        return self.rows

    def getFullDataIndex(self):
        returnValue = []

        for i in range(0,self.rows):
            returnValue.append(i)

        return returnValue



class DataObject:
    def __init__(self, fileName, sCols, iCols, fCols, target, defaults):
        self.file_name = fileName

        self.string_data = []
        self.int_data = []
        self.float_data = []

        self.refIDs = []

        self.target_data = []

        self.default_dict = defaults

        #self.problem_size = 0

        self.string_columns = sCols
        self.int_columns = iCols
        self.float_columns = fCols

        self.rows = 0;
        self.target_label = target



    def generate_defaults(self, data_matrix, column_names):
        row_len = len(column_names)
        data_len = len(data_matrix)
        counter = 0
        for col_name in column_names:
            print("Generating default for column: " + col_name)
            if col_name in self.string_columns:
                # String columns
                self.default_dict[col_name] = 'NULL'

            elif col_name in self.int_columns:
                # Int columns

                categories = []
                for y in range(data_len):
                    if data_matrix[y][counter] not in categories:
                        if not (data_matrix[y][counter] == 'NULL'):
                            categories.append(int(data_matrix[y][counter]))

                max_cat = max(categories)
                max_cat += 1


                self.default_dict[col_name] = max_cat


                #self.default_dict[col_name] = 0

            elif col_name in self.float_columns:
                # Float columns

                '''
                tot = 0
                for y in range(data_len):
                    if not (data_matrix[y][counter] == 'NULL'):
                        tot += float(data_matrix[y][counter])

                average = float(tot)/data_len

                self.default_dict[col_name] = average
                '''
                self.default_dict[col_name] = 0.0

            counter = counter + 1

        print(self.default_dict)


    def get(self):
        data_matrix = []
        print("Reading file...")
        with open(self.file_name, 'r') as csvfile:
            data_reader = csv.reader(csvfile)

            for row in data_reader:
                data_matrix.append(row)

        column_names = data_matrix[0]
        del data_matrix[0]
        print("Generating defaults")

        if self.default_dict == None:
            self.default_dict = {}
            self.generate_defaults(data_matrix, column_names)


        print("Generated defaults")
        sanity_counter = 0

        str_data = []
        in_data = []
        flo_data = []

        #self.problem_size = len(data_matrix)
        self.y_values = {}

        self.rows = len(data_matrix)

        for row in data_matrix:

            if sanity_counter % 1000 == 0:
                print (sanity_counter)
            string_row = []
            int_row = []
            float_row = []

            for counter in range(len(row)):
                if column_names[counter] == 'RefId':
                    self.refIDs.append(row[counter])
                if column_names[counter] == self.target_label:
                    y_val =float(row[counter])
                    self.target_data.append(y_val)
                    #self.target_float_data = np.append(self.target_float_data , float(row[counter]))
                    self.y_values[y_val] = y_val
                else:
                    if column_names[counter] in self.string_columns:
                        if row[counter] == 'NULL':
                            string_row.append(self.default_dict[column_names[counter]])
                        else:
                            string_row.append(str(row[counter]))
                    if column_names[counter] in self.int_columns:
                        if row[counter] == 'NULL':
                            int_row.append(self.default_dict[column_names[counter]])
                        else:
                            int_row.append(int(row[counter]))

                    if column_names[counter] in self.float_columns:
                        if row[counter] == 'NULL':
                            float_row.append(self.default_dict[column_names[counter]])
                        else:
                            float_row.append(float(row[counter]))

            str_data.append(string_row)
            in_data.append(int_row)
            flo_data.append(float_row)
            sanity_counter += 1

        self.string_data = str_data
        self.int_data = in_data
        self.float_data = flo_data
