from EnsembleSystem import driver
from EnsembleSystem import EnsembleTestSystem
from DataObject import DataObject
from DataObject import BaseDataContainer

import random
import csv
import math
import os

'''
When it produces a good file, copy into a saved directory so we keep track of the good ones
'''

def check_a_file(file_name):
    data = []
    found_a_zero = False
    found_a_one = False
    with open(file_name, 'r') as csvfile:
        data_reader = csv.reader(csvfile)

        for row in data_reader:
            data.append(row)
        del data[0]

    for id, guess in data:
        if int(guess) == 1:
            found_a_one = True
        if int(guess) == 0:
            found_a_zero = True

        if found_a_one and found_a_zero:
            return True

    return False

def check_files(file_name):
    file_name_post = "no_post" + file_name
    file_name_nopost = "post" + file_name

    if check_a_file(file_name_post) and check_a_file(file_name_nopost):
        return True

    return False

def copy_files(file_name, directory):
    file_name_post = "no_post" + file_name
    file_name_nopost = "post" + file_name

    print(file_name_post)
    print(file_name_nopost)
    print(os.curdir)

    dir_path = os.path.dirname(os.path.realpath(__file__))
    dir_path += "/"
    print(dir_path)
    os.rename(dir_path + file_name_nopost, dir_path +directory + file_name_nopost)

    os.rename(dir_path +file_name_post, dir_path + directory + file_name_post)

    #os.system("mv " + file_name_post + " " + directory + file_name_post)
    #os.system("mv " + file_name_nopost + " " + directory + file_name_nopost)

def generate_random_parameters(min_val,max_val):
    strat_list = ["const"]
    strat_id = random.randrange(0, len(strat_list))

    num_classifiers = random.randrange(min_val,max_val)

    chosen_strategy = strat_list[strat_id]

    file_name = str(chosen_strategy) + "_" + str(num_classifiers) + ".csv"

    return chosen_strategy, num_classifiers, file_name

def GetAverageDefaults():
    test_average_defaults = {'PurchDate': 'NULL', 'MMRCurrentAuctionCleanPrice': 7361.133635822366, 'Nationality': 'NULL', 'TopThreeAmericanName': 'NULL', 'MMRAcquisitionRetailAveragePrice': 8548.388445192682, 'WarrantyCost': 1281.1057548196359, 'Transmission': 'NULL', 'WheelTypeID': 4, 'Auction': 'NULL', 'VehBCost': 6760.866483051716, 'MMRCurrentAuctionAveragePrice': 6094.421807953682, 'MMRAcquisitionAuctionCleanPrice': 7391.968361837107, 'VehYear': 2011, 'SubModel': 'NULL', 'MMRCurrentRetailAveragePrice': 8786.258279097461, 'VehOdo': 71576.3514484571, 'Color': 'NULL', 'Trim': 'NULL', 'AUCGUART': 'NULL', 'WheelType': 'NULL', 'Make': 'NULL', 'MMRAcquisitionAuctionAveragePrice': 6133.62444823126, 'IsOnlineSale': 2, 'Model': 'NULL', 'VehicleAge': 10, 'VNZIP1': 98065, 'PRIMEUNIT': 'NULL', 'MMRCurrentRetailCleanPrice': 10165.867247007616, 'MMRAcquisitonRetailCleanPrice': 9918.548873878499, 'Size': 'NULL'}

    average_defaults = {'PurchDate': 'NULL', 'MMRCurrentAuctionCleanPrice': 7358.783100174013, 'Nationality': 'NULL', 'TopThreeAmericanName': 'NULL', 'MMRAcquisitionRetailAveragePrice': 8494.938684351151, 'WarrantyCost': 1276.580984612855, 'Transmission': 'NULL', 'WheelTypeID': 4, 'Auction': 'NULL', 'VehBCost': 6730.934326212953, 'MMRCurrentAuctionAveragePrice': 6105.614773303372, 'MMRAcquisitionAuctionCleanPrice': 7371.81745063919, 'VehYear': 2011, 'SubModel': 'NULL', 'MMRCurrentRetailAveragePrice': 8737.846662921502, 'VehOdo': 71499.99591685736, 'Color': 'NULL', 'Trim': 'NULL', 'AUCGUART': 'NULL', 'WheelType': 'NULL', 'Make': 'NULL', 'MMRAcquisitionAuctionAveragePrice': 6127.397626844608, 'IsOnlineSale': 2, 'Model': 'NULL', 'VehicleAge': 10, 'VNZIP1': 99225, 'PRIMEUNIT': 'NULL', 'MMRCurrentRetailCleanPrice': 10101.597084252497, 'MMRAcquisitonRetailCleanPrice': 9848.498677774276, 'Size': 'NULL'}

    return average_defaults


def GetZeroedDefaults():

    test_zero_defaults = {'Make': 'NULL', 'MMRCurrentRetailAveragePrice': 0.0, 'Color': 'NULL', 'Model': 'NULL', 'MMRAcquisitionAuctionCleanPrice': 0.0, 'Nationality': 'NULL', 'MMRCurrentAuctionCleanPrice': 0.0, 'MMRCurrentAuctionAveragePrice': 0.0, 'PurchDate': 'NULL', 'MMRAcquisitonRetailCleanPrice': 0.0, 'Transmission': 'NULL', 'TopThreeAmericanName': 'NULL', 'IsOnlineSale': 2, 'WheelTypeID': 4, 'VehYear': 2011, 'VNZIP1': 98065, 'MMRAcquisitionAuctionAveragePrice': 0.0, 'VehicleAge': 10, 'Size': 'NULL', 'PRIMEUNIT': 'NULL', 'MMRAcquisitionRetailAveragePrice': 0.0, 'Trim': 'NULL', 'VehOdo': 0.0, 'MMRCurrentRetailCleanPrice': 0.0, 'Auction': 'NULL', 'AUCGUART': 'NULL', 'WheelType': 'NULL', 'WarrantyCost': 0.0, 'VehBCost': 0.0, 'SubModel': 'NULL'}
    zero_defaults ={'Make': 'NULL', 'MMRCurrentRetailAveragePrice': 0.0, 'Color': 'NULL', 'Model': 'NULL', 'MMRAcquisitionAuctionCleanPrice': 0.0, 'Nationality': 'NULL', 'MMRCurrentAuctionCleanPrice': 0.0, 'MMRCurrentAuctionAveragePrice': 0.0, 'PurchDate': 'NULL', 'MMRAcquisitonRetailCleanPrice': 0.0, 'Transmission': 'NULL', 'TopThreeAmericanName': 'NULL', 'IsOnlineSale': 2, 'WheelTypeID': 4, 'VehYear': 2011, 'VNZIP1': 99225, 'MMRAcquisitionAuctionAveragePrice': 0.0, 'VehicleAge': 10, 'Size': 'NULL', 'PRIMEUNIT': 'NULL', 'MMRAcquisitionRetailAveragePrice': 0.0, 'Trim': 'NULL', 'VehOdo': 0.0, 'MMRCurrentRetailCleanPrice': 0.0, 'Auction': 'NULL', 'AUCGUART': 'NULL', 'WheelType': 'NULL', 'WarrantyCost': 0.0, 'VehBCost': 0.0, 'SubModel': 'NULL'}

    return zero_defaults


def GetAllColumns():
    string_columns = ['PurchDate', 'Auction', 'Make', 'Model', 'Trim', 'SubModel', 'Color', 'Transmission', 'WheelType','Nationality', 'Size', 'TopThreeAmericanName', 'PRIMEUNIT', 'AUCGUART']
    int_columns = ['VehYear', 'VehicleAge', 'WheelTypeID', 'VNZIP1', 'IsOnlineSale']


    float_columns = ['VehYear','VehicleAge','WheelTypeID', 'VehOdo', 'MMRAcquisitionAuctionAveragePrice','MMRAcquisitionAuctionCleanPrice', 'MMRAcquisitionRetailAveragePrice','MMRAcquisitonRetailCleanPrice', 'MMRCurrentAuctionAveragePrice', 'MMRCurrentAuctionCleanPrice','MMRCurrentRetailAveragePrice', 'MMRCurrentRetailCleanPrice', 'VNZIP1', 'VehBCost', 'IsOnlineSale','WarrantyCost']

    columns = { "string_columns" : string_columns, "int_columns" : int_columns, "float_columns" : float_columns}
    return columns

def GetGoodColumns():
    string_columns = ['Auction']
    int_columns = ['IsOnlineSale']
    float_columns = ['VehicleAge', 'VehOdo', 'MMRAcquisitionAuctionAveragePrice', 'MMRAcquisitionRetailAveragePrice',
                     'MMRCurrentAuctionAveragePrice', 'MMRCurrentRetailAveragePrice', 'VehBCost', 'WarrantyCost']
    columns = { "string_columns" : string_columns, "int_columns" : int_columns, "float_columns" : float_columns}
    return columns



def Get75kDataSet(columns,defaults):

    training_object = DataObject("training.csv", columns["string_columns"], columns["int_columns"], columns["float_columns"], 'IsBadBuy',defaults)
    training_object.get()

    test_object = DataObject("test.csv", columns["string_columns"], columns["int_columns"], columns["float_columns"], 'IsBadBuy',defaults)
    test_object.get()

    return [training_object,test_object]


def Get5kDataSet(columns, defaults):
    training_object = DataObject("debug_training.csv", columns["string_columns"], columns["int_columns"],
                                 columns["float_columns"], 'IsBadBuy', defaults)
    training_object.get()

    test_object = DataObject("debug_test.csv", columns["string_columns"], columns["int_columns"], columns["float_columns"],
                             'IsBadBuy', defaults)
    test_object.get()

    return [training_object, test_object]

def Get1kDataSet(columns, defaults):
    training_object = DataObject("small_debug_training.csv", columns["string_columns"], columns["int_columns"],
                                 columns["float_columns"], 'IsBadBuy', defaults)
    training_object.get()

    test_object = DataObject("small_debug_test.csv", columns["string_columns"], columns["int_columns"], columns["float_columns"],
                             'IsBadBuy', defaults)
    test_object.get()

    return [training_object, test_object]

def Get100DataSet(columns, defaults):
    training_object = DataObject("tiny_debug_training.csv", columns["string_columns"], columns["int_columns"],
                                 columns["float_columns"], 'IsBadBuy', defaults)
    training_object.get()

    test_object = DataObject("tiny_debug_test.csv", columns["string_columns"], columns["int_columns"], columns["float_columns"],
                             'IsBadBuy', defaults)
    test_object.get()

    return [training_object, test_object]


def main():

    defaults = GetAverageDefaults()
    #defaults = GetZeroedDefaults()

    columns = GetAllColumns()
    #columns = GetGoodColumns()


    #[training_object,test_object] = Get100DataSet(columns,defaults)
    #[training_object,test_object] = Get1kDataSet(columns,defaults)
    #[training_object,test_object] = Get5kDataSet(columns,defaults)
    [training_object,test_object] = Get75kDataSet(columns,defaults)

    min_estimators = 5
    max_estimators = 25

    sample_size = int(training_object.rows * 0.02)
    min_data = int(sample_size /2)

    estimators_string = "_min_est_" + str(min_estimators)
    estimators_string += "_max_est_" + str(max_estimators)


    min_classes = 5
    max_classes = 25

    classifier_string = "_min_class_" + str(min_classes)
    classifier_string += "_max_class_" + str(max_classes)

    max_depth = 4
    #total_classifiers = 100
    while True:

        strategy, total_classifiers, fout = generate_random_parameters(min_classes,max_classes)

        fout = "tot_class_" + str(total_classifiers) + estimators_string + classifier_string + "_min_data_" + str(min_data)
        fout += "_avg_defaults_"

        print("New test generated")
        print("    STRATEGY: " + strategy)
        print("    NUMBER OF CLASSIFIERS: " + str(total_classifiers))

        driver(fout, training_object, test_object, min_estimators, max_estimators,min_data,sample_size,max_depth,total_classifiers)

        if check_files(fout):
            print("Good Sample")
            copy_files(fout, "GoodFiles/")
        else:
            print("Bad Sample")
            copy_files(fout, "BadFiles/")

        break

        



if __name__ == "__main__":
    main()
