from EnsembleSystem import driver
from DataObject import DataObject
import random
import csv
import os

'''
When it produces a good file, copy into a saved directory so we keep track of the good ones
'''

def check_a_file(file_name):
    data = []
    found_a_zero = False
    found_a_one = False
    with open(file_name, 'r') as csvfile:
        data_reader = csv.reader(csvfile)

        for row in data_reader:
            data.append(row)
        del data[0]

    for id, guess in data:
        if int(guess) == 1:
            found_a_one = True
        if int(guess) == 0:
            found_a_zero = True

        if found_a_one and found_a_zero:
            return True

    return False

def check_files(file_name):
    file_name_post = "no_post" + file_name
    file_name_nopost = "post" + file_name

    if check_a_file(file_name_post) and check_a_file(file_name_nopost):
        return True

    return False

def copy_files(file_name, directory):
    file_name_post = "no_post" + file_name
    file_name_nopost = "post" + file_name

    print(file_name_post)
    print(file_name_nopost)
    print(os.curdir)

    dir_path = os.path.dirname(os.path.realpath(__file__))
    dir_path += "/"
    print(dir_path)
    os.rename(dir_path + file_name_nopost, dir_path +directory + file_name_nopost)

    os.rename(dir_path +file_name_post, dir_path + directory + file_name_post)

    #os.system("mv " + file_name_post + " " + directory + file_name_post)
    #os.system("mv " + file_name_nopost + " " + directory + file_name_nopost)

def generate_random_parameters():
    strat_list = ["const"]
    strat_id = random.randrange(0, len(strat_list))

    num_classifiers = random.randrange(10, 100)

    chosen_strategy = strat_list[strat_id]

    file_name = str(chosen_strategy) + "_" + str(num_classifiers) + ".csv"

    return chosen_strategy, num_classifiers, file_name

def main():
    string_columns = ['PurchDate','Auction','Make','Model','Trim','SubModel','Color','Transmission','WheelType','Nationality','Size','TopThreeAmericanName','PRIMEUNIT','AUCGUART']
    int_columns = ['VehYear','VehicleAge','WheelTypeID','VNZIP1','IsOnlineSale']
    float_columns = ['VehYear','VehicleAge','WheelTypeID','VehOdo','MMRAcquisitionAuctionAveragePrice','MMRAcquisitionAuctionCleanPrice','MMRAcquisitionRetailAveragePrice','MMRAcquisitonRetailCleanPrice','MMRCurrentAuctionAveragePrice','MMRCurrentAuctionCleanPrice','MMRCurrentRetailAveragePrice','MMRCurrentRetailCleanPrice','VNZIP1','VehBCost','IsOnlineSale','WarrantyCost']

    training_object = DataObject("training.csv", string_columns, int_columns, float_columns, 'IsBadBuy')
    training_object.get()

    test_object = DataObject("test.csv", string_columns, int_columns, float_columns, 'IsBadBuy')
    test_object.get()

    min_estimators = 10
    max_estimators = 100

    while True:
        strategy, num_class, fout = generate_random_parameters()

        print("New test generated")
        print("    STRATEGY: " + strategy)
        print("    NUMBER OF CLASSIFIERS: " + str(num_class))

        driver(strategy, num_class, fout, training_object, test_object, min_estimators, max_estimators)

        if check_files(fout):
            copy_files(fout, "GoodFiles/")
        else:
            copy_files(fout, "BadFiles/")




if __name__ == "__main__":
    main()
