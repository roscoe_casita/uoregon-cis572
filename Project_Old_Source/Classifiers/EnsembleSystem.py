from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import AdaBoostClassifier

import numpy as np
import math
import csv
import json
import random
from DataObject import BaseDataContainer
from DataObject import DataObject

class ClassifierGenerator:
    def __init__(self,min_est,max_est):
        self.min_estimators = min_est
        self.max_extimators = max_est

    def generateClassifier(self):
        return AdaBoostClassifier(n_estimators=random.randrange(self.min_estimators, self.max_extimators ))


class PredictorNode:
    def __init__(self,classifier_generator, y_values, min_data,sample_size,max_tree_depth):
        min_data= int(min_data)
        sample_size = int(sample_size)
        self.classifier_generator = classifier_generator
        self.classifier = self.classifier_generator.generateClassifier()

        self.y_val_posterior = dict()
        for y in y_values:
            self.y_val_posterior[y] = 0.0

        self.y_total_posterior = 0.0

        self.y_values = y_values

        self.children = None
        self.min_data = min_data
        self.sample_size=sample_size
        self.max_tree_depth = max_tree_depth

        self.y_val_numerator = dict()
        for y in y_values:
            self.y_val_numerator [y] = 0.0

        self.y_val_denominator = dict()
        for y in y_values:
            self.y_val_denominator[y] = 0.0

    def sub_sample_data(self,data_indexes):

        returnValue = []

        length = len(data_indexes)

        for i in range(self.sample_size):
            index = data_indexes[random.randint(0,length-1)]
            returnValue.append(index)

        return returnValue


    def build_and_train_on_data(self,data_indexes,data_container,result_container):

        data_index_subset = None

        data_index_subset = self.sub_sample_data(data_indexes)

        training_set = data_container.getDataSubset(data_index_subset )
        training_test_set = result_container.getDataSubset(data_index_subset )


        internal_node ="Tree internal node: "
        leaf_node ="Tree leaf node reached: "


        self.classifier.fit(training_set,training_test_set)

        if (self.max_tree_depth <= 0 ) or  (len(data_indexes) < self.min_data):
            print(leaf_node+ str(self.max_tree_depth) + " Data Prediction node Data with: " + str(len(data_index_subset)) + " Over Data element size: " + str(len(data_indexes)))
            return

        data_divider = {}
        for y in self.y_values:
            data_divider[y] = []


        data = data_container.getDataSubset(data_indexes)

        predictions = self.classifier.predict(data)

        for i in range(len(data_indexes)):

            prediction = predictions[i]

            index = data_indexes[i]

            data_divider[prediction].append(index)

        str_print = " "
        for y in self.y_values:
            str_print += str(y) + " : " + str(len(data_divider[y])) + " "
            if len(data_divider[y]) <= self.min_data:
                print(leaf_node+ str(self.max_tree_depth) + " sample size: " + str(len(data_index_subset)) + " data size: " + str(len(data_indexes)))
                return

        print(internal_node + str(self.max_tree_depth) + " sample size: " + str(len(data_index_subset)) + " data size: " + str(len(data_indexes)) + str_print)
        self.children = {}
        for y in self.y_values:
            self.children[y] = PredictorNode(self.classifier_generator,self.y_values,self.min_data,self.sample_size,self.max_tree_depth-1)

        for y in self.y_values:
            self.children[y].build_and_train_on_data(data_divider[y],data_container,result_container)

        return

    def metaRate(self, datas, answers):

        predictions = self.classifier.predict(datas)

        if self.children is not None:
            data_split = {}
            answers_split = {}

            for y in self.y_values:
                data_split[y] = []
                answers_split[y] = []

            for i in range(len(datas)):
                p = predictions[i]
                a = answers[i]
                data_split[p].append(datas[i])
                answers_split[p].append(answers[i])

            for y in self.y_values:
                if len(data_split[y]) != 0:
                    self.children[y].metaRate(data_split[y],answers_split[y])
                else:
                    print ("Found empty branch in meta rating.")

        else:
            for i in range(len(datas)):
                p = predictions[i]
                a = answers[i]
                if p == a:
                    self.y_val_numerator[a] += 1.0
                self.y_val_denominator[a] += 1.0

    def predict(self,data_set, indexes,result):
        predictions = self.classifier.predict(data_set)


        if self.children is not None:
            data_split = {}
            indexes_split = {}

            for y in self.y_values:
                data_split[y] = []
                indexes_split[y] = []

            for i in range(len(indexes)):
                p = predictions[i]

                data = data_set[i]
                data_split[p].append(data)
                index = indexes[i]
                indexes_split[p].append(index)

            for y in self.y_values:
                if len(data_split[y]) != 0:
                    self.children[y].predict(data_split[y],indexes_split[y],result)
                else:
                    print ("Found empty branch in predictions.")

        else:
            for i in range(len(indexes)):
                index = indexes[i]
                prediction = predictions[i]

                resultArray = [prediction]
                for y in self.y_values:
                    resultArray.append(self.y_val_posterior[y])
                resultArray.append(self.y_total_posterior)

                result[index] = resultArray


    def calc_posterior_distribution(self, total_data_size):

        if self.children is not None:
            for y in self.y_values:
                self.children[y].calc_posterior_distribution(total_data_size)
                self.y_val_numerator = None;
                self.y_total_posterior = None;
                self.y_val_posterior = None;

        else:
            total_count = 0.0

            for y in self.y_values:
                total_count += self.y_val_denominator[y]
                self.y_val_denominator[y] += 1.0
                y_post = self.y_val_numerator[y]/ self.y_val_denominator[y]
                self.y_val_posterior[y] = y_post

            self.y_total_posterior = total_count / total_data_size



class PredictorTree:
    def __init__(self,y_values,min_est,max_est,min_data,sample_size,max_depth):
        self.cg = ClassifierGenerator(min_est,max_est)
        self.root_node =PredictorNode(self.cg,y_values,min_data,sample_size,max_depth)
        self.y_values = y_values

    def trainPredictorTree(self,data_container,result_container):
        data_index = data_container.getFullDataIndex()

        self.root_node.build_and_train_on_data(data_index,data_container,result_container)

        full_data = data_container.complete_set
        full_answers = result_container.complete_set

        print("Calculating Meta Ratings.")
        self.root_node.metaRate(full_data,full_answers)

        print("Comute final posterior distributions.")
        self.root_node.calc_posterior_distribution(len(full_data))

    def predict(self,dataset):


        result_set = []
        index_set = []
        for i in range(len(dataset)):
            result_set.append([])
            index_set.append(i)

        self.root_node.predict(dataset,index_set,result_set)

        return result_set


class NeuralNetworkEnsemble:
    def __init__(self,problem_size, y_values):
        num_neurons_base = []
        '''
        if neuron_strategy == "log":
            # Log for no posterior
            while problem_size > 1:
                num_neurons_base.append(int(math.ceil(problem_size)))
                problem_size = math.log2(problem_size)
            num_neurons_base.append(1) # Add a final 1 to the end for a single output
        '''
        '''
        if neuron_strategy == "lin":
            num_neurons_base = []

            # incremental for no posterior
            while problem_size > 0:
                num_neurons_base.append(problem_size)
                problem_size -= 1
        '''

        #if neuron_strategy == "const":
        num_neurons_base = [problem_size,y_values,  1]
        h_layers_base = tuple(num_neurons_base)

        self.predictor = MLPClassifier(hidden_layer_sizes=h_layers_base, random_state=1)
        self.input_width = problem_size

    def train_on_predictions(self,matrix_of_predictions,prediction_vector):
        self.predictor.fit(matrix_of_predictions,prediction_vector)

    def predict(self,matrix_of_predictions):
        return self.predictor.predict(matrix_of_predictions)



class EnsembleTestSystem:
    def __init__(self, fname: str, min_est, max_est, min_data, sample_size,max_depth, total_classifiers, y_values):
        assert isinstance(fname, str)
        self.file_name = fname

        self.total_classifiers = total_classifiers

        self.classifiers = []
        self.y_values = y_values

        for i in range(0,total_classifiers):
            pt = PredictorTree(y_values,min_est,max_est,min_data,sample_size,max_depth)
            self.classifiers.append(pt)

        self.no_post_dist = NeuralNetworkEnsemble(total_classifiers)
        self.post_dist = NeuralNetworkEnsemble(total_classifiers)    #  * (len(y_values) + 2 ))

        # y^, p(y^=y | Y =y) , total mag of this predic of the picture.

        self.predictions_no_post = []
        self.predictions_with_post = []
        self.one_adaboost = None

    def train_system(self, data_object):
        print("Training Classifiers...")

        data_container = BaseDataContainer(data_object.float_data)
        result_container = BaseDataContainer(data_object.target_data)

        for i in range(0,self.total_classifiers):
            print("Training classifier " + str(i+1) + " of " + str(self.total_classifiers))
            self.classifiers[i].trainPredictorTree(data_container,result_container)

        [y_pred_matrix_without_post, y_pred_matrix_with_post] = self.tree_predict(data_container.complete_set)

        print("Training neural network without distributions.")
        self.no_post_dist.train_on_predictions(y_pred_matrix_without_post, data_object.target_data)
        print("Training neural network with augmented distributions.")
        self.post_dist.train_on_predictions(y_pred_matrix_with_post, data_object.target_data)


    def tree_predict(self,data_set):

        tree_width = 1 + len(self.y_values) + 1
        num_trees = len(self.classifiers)
        y_pred_matrix_width = tree_width * num_trees

        print("Building a forest of trees predictions.")
        tree_predictions = []

        classes = len(self.classifiers)
        for i in range(classes):
            print ("Building prediction set " + str(i) + " of " + "str" + str(classes))
            classifier = self.classifiers[i]
            prediction = classifier.predict(data_set)
            tree_predictions.append(prediction)

        print("Building y hats and y hats with posterior distribution from predictions")
        y_pred_matrix_without_post = []
        y_pred_matrix_with_post = []

        for i in range(len(data_set)):

            no_post_dist_vect = []
            for pd in range( self.no_post_dist.input_width):
                no_post_dist_vect.append(0.0)

            y_pred_matrix_without_post.append(no_post_dist_vect)


            post_dist_vect = []
            for pd in range(y_pred_matrix_width ):
                post_dist_vect.append(0.0)

            y_pred_matrix_with_post.append(post_dist_vect)


        for tree_index in range(len(tree_predictions)):

            tree_prediction = tree_predictions[tree_index]
            y_post_index = tree_index * tree_width

            for data_index in range(len(data_set)):

                #print("Tree index: " + str(tree_index) + " Data index: " + str(data_index))

                vect_values = tree_prediction[data_index]

                float_value = vect_values[0]
                y_pred_matrix_without_post[data_index][tree_index] = float_value

                y_data_vect = y_pred_matrix_with_post[data_index]

                di = 0
                y_data_vect[y_post_index + di] = vect_values[di]
                di = di + 1

                for y in self.y_values :
                    y_data_vect[y_post_index + di] = vect_values[di]
                    di = di +1
                y_data_vect[y_post_index + di] = vect_values[di]


        return  [y_pred_matrix_without_post,y_pred_matrix_with_post]

    def system_predict(self, test_object):
        print("Making predictions...")



        data_container = BaseDataContainer(test_object.float_data)

        print("Building tree for nn predictions.")

        [y_pred_matrix_without_post, y_pred_matrix_with_post] = self.tree_predict(data_container.complete_set)


        print("Predicting NN no post ")
        self.predictions_nopost = self.no_post_dist.predict(y_pred_matrix_without_post)
        print("Predicting NN with posterior distributions.")
        self.predictions_with_post = self.post_dist.predict(y_pred_matrix_with_post)


    def write_data(self, test_object):
        with open("no_post" + self.file_name, 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['RefID', 'IsBadBuy'])
            for x in range(test_object.rows):
                myList = [test_object.refIDs[x], int(self.predictions_nopost[x])]
                writer.writerow(myList)

        with open("post" + self.file_name, 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['RefID', 'IsBadBuy'])
            for x in range(test_object.rows):
                myList = [test_object.refIDs[x], int(self.predictions_with_post[x])]
                writer.writerow(myList)

    def train_one_10k_adaboost(self,data_object):
        self.one_adaboost = AdaBoostClassifier(n_estimators=10000)

        data_container = BaseDataContainer(data_object.float_data)
        result_container = BaseDataContainer(data_object.target_data)

        data_index = data_container.getFullDataIndex()

        data = data_container.getDataSubset(data_index)
        result = result_container.getDataSubset(data_index)


        self.one_adaboost.fit(data,result)
        return

    def predict_10k_adaboost(self,data_object):

        data_container = BaseDataContainer(data_object.float_data)
        data_index = data_container.getFullDataIndex()
        data = data_container.getDataSubset(data_index)

        predictions = self.one_adaboost.predict(data)
        self.predictions_with_post = predictions
        self.predictions_nopost = predictions
        return

def driver(file_out, training_object, test_object, min_estimators, max_estimators,min_data,sample_size,max_depth,total_classifiers):

    #columns = len(training_object.float_data[0])

    keys = training_object.y_values.keys()

    test_ensemble_system = EnsembleTestSystem(file_out,min_estimators,max_estimators,min_data,sample_size,max_depth,total_classifiers,keys)

    #test_ensemble_system.train_system(training_object)
    test_ensemble_system.train_one_10k_adaboost(training_object)

    #test_ensemble_system.system_predict(test_object)
    test_ensemble_system.predict_10k_adaboost(test_object)

    test_ensemble_system.write_data(test_object)
