#!/bin/bash

echo Clean the test directory of output files.
rm id3
rm data1/data1.*
rm data2/data2.*
rm data3/mushroom_test.*
rm data4/tennis_test.*

echo
echo Move to the release directory and build.
echo  

cd machine_learning/Release/
make clean
make all
cd ..
cd ..

echo
echo Build the documentation pages. 
echo
echo doxygen
echo cd documentation/latex/
echo make
echo cd ..
echo cd ..
echo cp documentation/latex/refman.pdf documentation.pdf


echo
echo Copy the output Release binary to the main directory.
echo

cp machine_learning/Release/machine_learning id3

echo
echo Running program, test, generate model and visualization now.
echo 

./id3 data1/training_set.csv data1/validation_set.csv data1/data1.model
./id3 data2/training_set.csv data2/validation_set.csv data2/data2.model
./id3 data3/agaricuslepiotatrain1.csv data3/agaricuslepiotatest1.csv data3/mushroom_test.model
./id3 data4/tennis.csv data4/tennis.csv data4/tennis_test.model

echo
echo Look at the data4/tennis_test.model file for multi-value-variables.
echo
cp data4/tennis_test.model tennis_test.model


echo
echo If you have meld installed, uncmment out for diff of output models.
echo
echo meld data1/dataset1.model data1/data1.model 
echo meld data2/dataset2.model data2/data2.model 
echo meld data3/mushroom.model data3/mushroom_test.model 

echo 
echo If you have xdot installed, uncomment out the line to see visualization.
echo
echo xdot data1/data1.model.dot 
echo xdot data2/data2.model.dot 
echo xdot data3/mushroom_test.model.dot 
echo xdot data4/tennis_test.model.dot


echo
echo The example splits on the multi-variable value type overcast.
echo if you have dot installed, uncomment out the line to build the example.
echo

echo dot data1/data1.model.dot -Tpdf -odata1.pdf
echo dot data2/data2.model.dot -Tpdf -odata2.pdf
echo dot data3/mushroom_test.model.dot -Tpdf -omushroom_test.pdf
echo dot data4/tennis_test.model.dot -Tpdf -otennis_test.pdf
