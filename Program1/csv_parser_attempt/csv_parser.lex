/* 
 This is the quack language lexical definition file for flex.



*/

%top{
#include <string>
#include <fstream>
#include <vector>

typedef struct _CSVFile{
	std::vector<std::string> ColumnNames;
	std::vector<std::vector<bool> > RowsOfColumns;
	std::vector<std::vector<bool> > ColumnsOfRows;
} CSVFile;


CSVFile *ParseFile(std::string filename);
void WriteFile(std::string filename, CSVFile *csv);

}

%{

CSVFile *CSV;
//http://aquamentus.com/flex_bison.html
int CurrentRow =0;
int CurrentColumn =0;


%}

comma		,
notcomma	[^,\r\n]
endline		[\r\n]+
false		0
true		1
%option yylineno noyywrap
%option outfile="csv_parser.cc" header-file="csv_parser.h"
%%
false		{ CSV->RowsOfColumns[CurrentRow].push_back(false); return 1;}
true		{ CSV->RowsOfColumns[CurrentRow].push_back(true); return 1;}
comma		{ CurrentColumn++;return 1;}
notcomma+	{ CSV->ColumnNames.push_back(std::string(yytext)); return 1;}
endline		{ 
			CurrentRow++; 
			CurrentColumn = 0 ; 
			CSV->RowsOfColumns.push_back(std::vector<bool>());
			return 1;
		}

<<EOF>>		{ return 0; }

%%

CSVFile *ParseFile(std::string filename)
{
	CSV = new CSVFile();

	yyin = fopen (filename.c_str(), "r");
	
	while( 0 != yylex())
	{
	}

	return CSV;
}

void WriteFile(std::string filename, CSVFile *csv)
{
	std::fstream file;
	file.open(filename.c_str(),std::fstream::out);

	size_t col_count = csv->ColumnNames.size();
	size_t row_count = csv->RowsOfColumns.size();

	for(size_t i = 0; i < col_count; i++)
	{
		file << csv->ColumnNames[i];
		if( i+1 < col_count)
		{
			file << ",";
		}
	}
	file << std::endl;

	for(size_t i = 0; i < row_count; i++)
	{
		for(size_t j =0; j < col_count; j++)
		{
			if( csv ->RowsOfColumns[i][j])
			{
				file << "1";
			}
			else
			{
				file << "0";
			}
			if(j +1 < col_count)
			{
				file << ",";
			}
		}
		file << std::endl;
	}
	file.close();
}


int main( int argc, char *argv[] )
{
	CSVFile *file1,*file2,*file3;

	file1 = ParseFile(argv[1]);

	WriteFile(argv[2],file1);

}

