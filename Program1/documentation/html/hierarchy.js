var hierarchy =
[
    [ "_CSVFile", "struct___c_s_v_file.html", null ],
    [ "DecisionTreeBuilder", "class_decision_tree_builder.html", [
      [ "ID3TreeBuilder", "class_i_d3_tree_builder.html", null ]
    ] ],
    [ "GraphVizNodeDecorator", "class_graph_viz_node_decorator.html", [
      [ "DecisionNode", "class_decision_node.html", [
        [ "DecisionTreeNode", "class_decision_tree_node.html", null ],
        [ "TerminalLeaf", "class_terminal_leaf.html", null ]
      ] ]
    ] ],
    [ "IdentifierDatabase", "class_identifier_database.html", null ],
    [ "VariableEdge", "struct_variable_edge.html", null ]
];