var searchData=
[
  ['children',['Children',['../class_decision_node.html#a37b65547225b3638f14d9affdca1cf3b',1,'DecisionNode']]],
  ['classify',['Classify',['../class_decision_node.html#aa6995cc07e16f707942f8437e76e88fa',1,'DecisionNode::Classify()'],['../class_terminal_leaf.html#a741e67ebdbdb82986964a2cb27a1ddd8',1,'TerminalLeaf::Classify()'],['../class_decision_tree_node.html#ab80e90d09def369c538d2a708076eb76',1,'DecisionTreeNode::Classify()']]],
  ['column_5findex',['column_index',['../class_decision_tree_node.html#a062bca903190759c7e45260e34e77716',1,'DecisionTreeNode']]],
  ['columnnames',['ColumnNames',['../struct___c_s_v_file.html#a40f747a9e93595205f683adb5e0fa6d0',1,'_CSVFile']]],
  ['csv_5ffile',['csv_file',['../class_decision_tree_builder.html#a4ff8a4eb1e458f5e05b01ea0b98b5058',1,'DecisionTreeBuilder']]],
  ['csv_5fparser_2ecpp',['csv_parser.cpp',['../csv__parser_8cpp.html',1,'']]],
  ['csv_5fparser_2eh',['csv_parser.h',['../csv__parser_8h.html',1,'']]],
  ['csvfile',['CSVFile',['../csv__parser_8h.html#a05a9dfcc8712ab5de3b5c832dd553f81',1,'csv_parser.h']]]
];
