################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/DecisionTreeNode.cpp \
../src/GraphVizNodeDecorator.cpp \
../src/csv_parser.cpp \
../src/machine_learning.cpp 

OBJS += \
./src/DecisionTreeNode.o \
./src/GraphVizNodeDecorator.o \
./src/csv_parser.o \
./src/machine_learning.o 

CPP_DEPS += \
./src/DecisionTreeNode.d \
./src/GraphVizNodeDecorator.d \
./src/csv_parser.d \
./src/machine_learning.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


