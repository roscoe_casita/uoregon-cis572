/*	Roscoe Casita.
 *  University of Oregon
 *  Winter 2017, CIS 572 Machine Learning.
 */
#include "main.h"


/*
 * This is the driver for the top level function.
 * There is no error checking in this system.
 */
int main( int argc, char *argv[])
{
	if(argc != 4)
	{
		printf("Usage:  ./id3 <training_data.csv> <validation_data.csv> <output.model>\r\n");
		return -1;
	}
	CSVFile *file1,*file2;

	file1 = ReadCSVFile(argv[1]);
	file2 = ReadCSVFile(argv[2]);

	DecisionTreeBuilder *builder = NULL;

	builder = new ID3TreeBuilder(file1);

	//builder = new J48C45(file1);


	DecisionTreeNode *ptr = builder->BuildDecisionTree();


	std::string output = GenerateGraphviz(ptr,true,argv[1]);

	std::string graphviz = argv[3];
	graphviz += ".dot";
	std::ofstream graphviz_file(graphviz.c_str());
	graphviz_file << output;
	graphviz_file.close();

	std::string model = ptr->GenerateModelFile(0);

	std::ofstream model_file(argv[3]);
	model_file << model;
	model_file.close();

	float correct =0.0;
	size_t row_count = file2->RowsOfColumns.size();
	size_t col_count = file2->ColumnNames.size();
	float count = (float)row_count;

	for(size_t i =0; i < row_count;i++)
	{
		DecisionNode *term = ptr->Classify( file2->RowsOfColumns[i] );

		std::string result = term->VariableName;
		std::string test = file2->RowsOfColumns[i][col_count-1];

		if(result.compare(test)==0)
		{
			correct += 1.0;
		}
	}

	correct = correct / count;

	delete ptr;
	delete builder;
	delete file2;
	delete file1;

	std::cout<< "Accuracy: " << correct << std::endl;



}
