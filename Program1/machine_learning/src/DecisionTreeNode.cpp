/*
 * DecisionTreeNode.cpp
 *
 *  Created on: Jan 28, 2017
 *      Author: roscoecasita
 */

#include "DecisionTreeNode.h"
#include <math.h>


DecisionNode::DecisionNode(std::string node_name):VariableName(node_name)
{
	this->NodeLabel = node_name;
}
DecisionNode::~DecisionNode()
{
	this->NodeLabel ="";
}




/*
 * Customize the graphviz strings for generation later.
 * they also serve as the node value, in this case for answers like "yes","no","1","0".
 */
TerminalLeaf::TerminalLeaf(std::string terminal_value):DecisionNode(terminal_value),TerminalValue(terminal_value)
{
	this->NodeStyle = "circle";
}

/*
 * Just a string.
 */
TerminalLeaf::~TerminalLeaf()
{
	this->NodeStyle ="";
}

/*
 * Generate a useless set to enumerate.
 */
std::vector<GraphVizNodeDecorator *> TerminalLeaf::GenerateGraphvizChildren()
{
	return std::vector<GraphVizNodeDecorator *> ();
}

DecisionNode *TerminalLeaf::Classify(std::vector<std::string> instance)
{
	return this;
}
/*
 * Remember the variable name, set the graphviz style and the column index.
 */
DecisionTreeNode::DecisionTreeNode(std::string name,size_t column):DecisionNode(name)
{
	this->NodeStyle = "box";
	this->column_index = column;
}

/*
 * If I am being trimmed, so are my children.
 */
DecisionTreeNode::~DecisionTreeNode()
{
	this->NodeStyle = "";
	this->column_index = 0;
	for(size_t i =0; i < Children.size(); i++)
	{
		delete Children[i]->Target;
		Children[i]->Target =0;

		delete Children[i];
		Children[i] =0;
	}
	Children.empty();
}

/*
 * Add a child to the tree, this is the dynamic part, it should be possible some where.
 */
void DecisionNode::AddChild(VariableEdge *new_child)
{
	Children.push_back(new_child);
}

/*
 * This generates an enumeration of the sub nodes.
 */
std::vector<GraphVizNodeDecorator *> DecisionNode::GenerateGraphvizChildren()
{
	std::vector<GraphVizNodeDecorator *> returnValue;

	for(size_t i = 0; i < Children.size(); i++)
	{
		returnValue.push_back( Children[i]->Target);
	}
	return returnValue;
}

/*
 * This should be the last step to generate edge labels.
 */
std::string DecisionNode::GenerateGravizEdgeDecoration(GraphVizNodeDecorator *edge)
{
	std::ostringstream returnValue;
	for(size_t i =0; i< Children.size(); i++)
	{
		if(Children[i]->Target ==edge)
		{
			returnValue <<  "[label =\"" << Children[i]->VariableValue << "\\n";

			if(Children[i]->EdgeLabel.compare("")!=0)
			{
				returnValue << Children[i]->EdgeLabel + "\\n";
			}

			returnValue << "\"]";
			break;
		}
	}
	return returnValue.str();
}

/*
 * Helper function to generate each line.
 */
std::string GenerateLine(size_t depth, std::string Variable, std::string VariableValue, std::string Result)
{
	std::string returnValue;

	for(size_t i=0; i<depth; i++)
	{
		returnValue += "| ";
	}
	returnValue += Variable + " = " + VariableValue + " :";

	if(Result.compare("")!=0)
	{
		returnValue += " ";
		returnValue += Result;
	}
	returnValue +="\r";
	return returnValue;
}

/*
 * Generate the required model file for class, CIS-572.
 *
 */
std::string DecisionTreeNode::GenerateModelFile(size_t depth)
{
	std::string Variable = this->VariableName;
	std::string VariableValue;
	std::string Result;
	std::string returnValue;

	for(size_t i=0; i < Children.size(); i++)
	{
		VariableEdge *child = Children[i];

		VariableValue = (*child).VariableValue;

		TerminalLeaf *test = dynamic_cast<TerminalLeaf*>((*child).Target);

		if(NULL != test )
		{
			Result = test->TerminalValue;
			returnValue += GenerateLine(depth,Variable,VariableValue,Result);
		}
		else
		{
			Result = "";
			returnValue += GenerateLine(depth,Variable,VariableValue,Result);

			DecisionTreeNode *ptr = dynamic_cast<DecisionTreeNode *>((*child).Target);
			returnValue += ptr->GenerateModelFile(depth +1);
		}
	}
	return returnValue;
}

/*
 * All the hard work is done,
 * Recursively walk the tree downwards and find a terminal node.
 */
DecisionNode *DecisionTreeNode::Classify(std::vector<std::string> instance)
{
	// Get the variable value from this instance.
	std::string value = instance[this->column_index];

	//Find the child whose edge contains the variable value.
	for(size_t i =0; i < this->Children.size(); i++)
	{
		VariableEdge *edge =this->Children[i];
		if( edge->VariableValue.compare(value) ==0)
		{
			// Incrementally updating will be the next stage.

			return edge->Target->Classify(instance);
		}
	}
	return NULL;
}


/*
 * This decision tree builder / id3 really,
 *
 * is build around using lots of binary / categorical feature sets.
 * using only a csv file with the last column equal to the relevant classification...
 *
 * The absence, or badness of data is not checked at this level. Cleaning functions should do that before this.
 *
 */
DecisionTreeBuilder::DecisionTreeBuilder(CSVFile *csv)
{
	csv_file = csv;

	size_t columns = csv_file->ColumnNames.size();
	size_t rows = csv_file->RowsOfColumns.size();

	/*
	 * Initialize the map of column names to index and vice verse.
	 * these maps are helpful for digging the correct index of a variable out.
	 * each decision tree node stores one index and a variable value per edge.
	 *
	 */
	for(size_t c =0; c <columns; c++)
	{
		IndexMap[csv_file->ColumnNames[c]] = c;
		ReverseIndexMap[c] =csv_file->ColumnNames[c];

		VariableValues[csv_file->ColumnNames[c]] = std::vector<std::string>();
	}

	/*
	 * The domain of variables is not known until all values are accounted for.
	 * This is fairly uninteresting in binary values variable files.
	 */
	for(size_t r =0; r < rows; r++)
	{
		std::vector<std::string> &row = csv_file->RowsOfColumns[r];
		for(size_t c =0; c<columns; c++ )
		{
			std::string &var_name = ReverseIndexMap[c];
			std::string &var_value = row[c];
			std::vector<std::string> &var_values = VariableValues[var_name];
			std::vector<std::string>::iterator itr = find(var_values.begin(),var_values.end(),var_value);

			if(itr == var_values.end())
			{
				var_values.push_back(var_value);
			}
		}
	}
	/*
	 * Get the TargetVariable value.
	 * This was where the target variable index was as well.
	 *
	 */
	TargetVariable = csv_file->ColumnNames[columns-1];
	TargetIndex = IndexMap[TargetVariable];
}

/*
 * Just clean up the mess.
 */
DecisionTreeBuilder::~DecisionTreeBuilder()
{
	csv_file = 0;
}


/*
 *
 * Build a subspace divided mapping of variable domain values to classify the results.
 *
 * In this case an EXACT match is required between string values, this should be variable ranges for real numbers.
 *
 * This is also where the classifier selected, for the set, instead of just the variable
 *
 * call it... a ... variable classifier. this comes later.
 *
 */
std::map<std::string,std::vector<size_t> > DecisionTreeBuilder::SegmentSet(std::vector<size_t> class_set, std::string variable)
{
	size_t variable_index = this->IndexMap[variable];

	std::map<std::string,std::vector<size_t> >  returnValue;

	std::vector<std::string> variable_values = this->VariableValues[variable];

	/*
	 * Initialize the map of , for every variable value,
	 * create a storage area, to contain the indexes of the rows that are relevant from the given super space (class_set).
	 */
	for(size_t i=0; i< variable_values.size(); i++)
	{
		std::string variable_value = variable_values[i];
		returnValue[variable_value] = std::vector<size_t>();
	}

	/*
	 * Here, loop through each of the relevant elements in the super space, and divide them up
	 * one and only one sub space gets the element. Each step, the super space is subdivided up into tinier pieces.
	 *
	 */
	for(size_t i=0; i< class_set.size();i++ )
	{
		size_t index = class_set[i];

		std::vector<std::string> &row = this->csv_file->RowsOfColumns[index];

		std::string variable_value = row[variable_index];

		returnValue[variable_value].push_back(index);

	}
	return returnValue;
}

/*
 *
 * Calculate the standard entropy decrease for subdividing the domain by the variable values
 * gain for selecting a variable.
 *
 * This requires that the space be first divided up into n-values (one for each value of the variable),
 *
 * then caluclate entropy for each of the subdivided spaces.
 *
 * sum the partial differences to calculate total gain to be used as a threshold.
 *
 */
double DecisionTreeBuilder::Gain(std::vector<size_t> class_set,std::string variable_name)
{
	/*
	 * Calculate full entropy for the set S.
	 */
	double returnValue = Entropy(class_set);
	double startset_size = (double) (class_set.size());

	// Using the selected variable, get the variable values.
	std::vector<std::string> variable_values = this->VariableValues[variable_name];

	// Segment the space map based upon the variable values.
	std::map<std::string, std::vector<size_t> >  variable_subspace_map = this->SegmentSet(class_set,variable_name);

	// calculate the entropy for each variable value, summing the partial values.
	for(std::vector<std::string>::iterator itr = variable_values.begin();itr != variable_values.end(); itr++)
	{
		std::vector<size_t> result_values = variable_subspace_map[*itr];

		//get the sub set size.
		double subset_size = (double)(result_values.size());

		// get the entropy for the sub set
		double result =  Entropy(result_values);

		// normalize the entropy for the sub set by the proportionality of the sub set to the total set size.
		double partial = (subset_size/startset_size) * result;

		returnValue = returnValue - partial;

	}
	// the gain should match 100% to the values in tennis.csv and the tom mitchell machine learning book.
	return returnValue;
}

/*
 * Calculate the sum of the parts divided by the whole, in such a way to calculate the
 *
 */
double DecisionTreeBuilder::Entropy(std::vector<size_t> class_set)
{
	if(class_set.size()==0)
		return 0.0;
	double returnValue = 0.0;
	double total_count = (double)(class_set.size());

	std::string result_name = this->TargetVariable;
	size_t result_index = this->IndexMap[result_name];
	std::vector<std::string> result_values = this->VariableValues[result_name];

	std::map<std::string,double> counters;

	/*
	 * Initialize the sum vector.
	 */
	for(size_t i =0; i < result_values.size();i++)
	{
		counters[result_values[i] ] = 0.0;
	}


	/*
	 * for each index, dereference the row out of the csv file table.
	 * get the result value
	 *
	 * index into the counter vector, by the result value and increment the value.
	 */
	for(size_t i =0; i < class_set.size(); i++)
	{
		size_t index = class_set[i];
		std::vector<std::string> row = csv_file->RowsOfColumns[index];

		std::string result = row[result_index];

		counters[result] += 1.0;
	}

	/*
	 * Calculate the entropy as the sum of ratios (partial count / total count) times the log base 2 (ratio) again.
	 *
	 * This function is returning the value decided upon at a higher level. this is used as a partial sum of a partial sum one tier up.
	 *
	 * instead of ever changing this or the other function, write a new deciding function and use that.
	 *
	 *  This is text book standard entry, Machine Learning, Tom M. Mitchell, page 57,.
	 */
	for(size_t i =0; i< result_values.size(); i++)
	{
		double P_i = counters[result_values[i]];

		if(P_i > 0.0)
		{
			P_i = P_i / total_count;

			double partial = (-1.0)*(P_i)*log2(P_i);
			returnValue += partial;
		}
	}
	return returnValue;
}

/*
 * Test to see if the set of class points is close enough to being 'done',
 *
 * In this case, the value must be 100% classified, no exceptions.
 *
 *
 */
bool DecisionTreeBuilder::IsTerminal(std::vector<size_t> class_set,std::string &terminal_value,double gain)
{
	bool returnValue = false;


	std::map<std::string,size_t> terminal_count;
	std::string result;

	/*
	 * Loop over all the points in the space, and use the values to dereference the rows
	 *
	 * Each row has a result value as the last index - this is just file format and prior knowledge.
	 *
	 * Index into the terminal vector and increase the count.
	 *
	 */
	for(std::vector<size_t>::iterator itr = class_set.begin();itr != class_set.end(); itr++)
	{
		result = this->csv_file->RowsOfColumns[(*itr)][this->TargetIndex];

		terminal_count[result] += 1;
	}

	/*
	 * If there are only result values of the same class, then the result set is terminal.
	 */
	if(terminal_count.size()==1)// ||(gain==1.0))
	{
		terminal_value = result;
		returnValue = true;
	}
	return returnValue;
}


/*
 *
 * Build the decision tree recursively, using a set of variables, and a space of indexes. Starting from the top and
 * work down.
 *
 *
 *
 */
DecisionTreeNode *ID3TreeBuilder::BuildID3Recursive(std::vector<std::string> variables,std::vector<size_t> indexes)
{
	DecisionTreeNode *returnValue = NULL;

	std::string variable;
	std::string variable_value;
	std::vector<size_t> variable_set;
	std::map<std::string,std::vector<size_t> >  variable_subspace_map;
	std::map<std::string,std::vector<size_t> >::iterator itr;

	double max = 0.0;
	double gain = 0.0;

	VariableEdge *edge=NULL;
	std::string terminal_value;


	std::ostringstream node_label;
	/*
	 * Calculate the gain for selecting the current variable.
	 *
	 * This is a very expensive operation.
	 *
	 *   Outer loop:  For Each Variable:			V
	 *   		Gain:	-Access The Rows:			R
	 *
	 *   Still as close to optimal as you can get.
	 *
	 *
	 */
	for(std::vector<std::string>::iterator itr = variables.begin();itr != variables.end();itr++)
	{
		gain = this->Gain(indexes,(*itr));
		if(gain > max)
		{
			variable = (*itr);
			max = gain;
		}

	}

	/*
	 * This can be made faster, we already segmented the set in gain,
	 * now its for real though, this ensures termination.

	 */

	if(max == 0.0)
	{
		terminal_value = this->csv_file->RowsOfColumns[indexes[0]][this->TargetIndex];
		returnValue = (DecisionTreeNode*)(new TerminalLeaf(terminal_value));
		node_label << TargetVariable << " = " << terminal_value;
		returnValue->NodeLabel = node_label.str();
	}
	else
	{

		returnValue = new DecisionTreeNode(variable, this->IndexMap[variable]);

		node_label << "E = " << this->Entropy(indexes) << "\\n";
		node_label << "G(" << variable << ")=" << max;

		returnValue->NodeLabel = node_label.str();

		variable_subspace_map = this->SegmentSet(indexes,variable);

		/*
		 * Loop over the sub spaces, and decide if they are terminal, or need further division.
		 */
		for(itr = variable_subspace_map.begin(); itr != variable_subspace_map.end(); itr++)
		{
			node_label.str(std::string());
			node_label.clear();

			variable_set = (*itr).second;
			variable_value = (*itr).first;

			edge = new VariableEdge();
			edge->VariableValue = variable_value;


			if(IsTerminal(variable_set,terminal_value,max))
			{
				edge->Target = new TerminalLeaf(terminal_value);
				node_label << TargetVariable << " = " << terminal_value;
				edge->Target->NodeLabel = node_label.str();
			}
			else
			{

				std::vector<std::string> new_variables = variables;

				std::vector<std::string>::iterator remove = std::find(new_variables.begin(),new_variables.end(),variable);

				new_variables.erase(remove);

				DecisionTreeNode *next = this->BuildID3Recursive(new_variables,variable_set);

				edge->Target = next;
			}
			// Attach the edge child node.
			returnValue->AddChild(edge);
		}
	}
	return returnValue;
}


/*
 * This is the top level builder function,
 *  it needs to initialize the variable and indexes first.
 *
 */
DecisionTreeNode *ID3TreeBuilder::BuildDecisionTree()
{
	std::vector<std::string> variables;
	std::vector<size_t> all_indexes;

	for(size_t i =0; i < csv_file->ColumnNames.size() -1 ;i++)
	{
		std::string variable = csv_file->ColumnNames[i];
		variables.push_back(variable);
	}

	for(size_t i =0; i < csv_file->RowsOfColumns.size();i++)
	{
		all_indexes.push_back(i);
	}

	return BuildID3Recursive(variables,all_indexes);
}

ID3TreeBuilder::ID3TreeBuilder(CSVFile *file):DecisionTreeBuilder(file)
{

}

ID3TreeBuilder::~ID3TreeBuilder()
{

}

/*
 * Implemented soon.
 *
J48C45::J48C45(CSVFile *file):DecisionTreeBuilder(file)
{

}
J48C45::~J48C45()
{

}
DecisionTreeNode *J48C45::BuildDecisionTree()
{
	DecisionTreeNode *returnValue = NULL;


	return returnValue;
}
DecisionTreeNode *J48C45::BuildJ48C45Recursive(std::vector<std::string> variables,std::vector<size_t> indexes)
{
	DecisionTreeNode *returnValue = NULL;


	return returnValue;

}
*/
