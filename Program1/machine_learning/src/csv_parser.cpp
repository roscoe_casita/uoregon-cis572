#include "csv_parser.h"

#include <algorithm>
#include <iterator>
#include <fstream>

using namespace std;

vector<string> split(const string& s, const string& delim, const bool keep_empty = true) {
    vector<string> result;
    if (delim.empty()) {
        result.push_back(s);
        return result;
    }
    string::const_iterator substart = s.begin(), subend;
    while (true) {
        subend = search(substart, s.end(), delim.begin(), delim.end());
        string temp(substart, subend);
        if (keep_empty || !temp.empty()) {
            result.push_back(temp);
        }
        if (subend == s.end()) {
            break;
        }
        substart = subend + delim.size();
    }
    return result;
}


CSVFile *ReadCSVFile(std::string filename)
{
	CSVFile *CSV = new CSVFile();

	std::ifstream file;
	file.open(filename.c_str());

	std::string line;

	std::getline(file,line);
	std::string delim = ",";
	CSV->ColumnNames = split(line,delim,true);

	//size_t col_count = CSV->ColumnNames.size();

	while(!file.eof())
	{
		std::getline(file,line);
		if(line.size()==0)
			break;

		vector<string> strings = split(line,delim,true);
		CSV->RowsOfColumns.push_back(strings);
	}
	file.close();
	return CSV;
}

void WriteCSVFile(std::string filename, CSVFile *csv)
{
	std::fstream file;
	file.open(filename.c_str(),std::fstream::out);

	size_t col_count = csv->ColumnNames.size();
	size_t row_count = csv->RowsOfColumns.size();

	for(size_t i = 0; i < col_count; i++)
	{
		file << csv->ColumnNames[i];
		if( i+1 < col_count)
		{
			file << ",";
		}
	}
	file << std::endl;

	for(size_t i = 0; i < row_count; i++)
	{
		for(size_t j =0; j < col_count; j++)
		{
			file << csv->RowsOfColumns[i][j];
			if(j +1 < col_count)
			{
				file << ",";
			}
		}
		file << std::endl;
	}
	file.close();
}
