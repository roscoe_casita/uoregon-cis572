/*
 * DecisionTreeNode.h
 *
 *  Created on: Jan 28, 2017
 *      Author: roscoecasita
 */

#ifndef DECISIONTREENODE_H_
#define DECISIONTREENODE_H_

#include "csv_parser.h"
#include <map>
#include "GraphVizNodeDecorator.h"

/*
 * This is the primitive structure to hold and edge with a variable -value.  (0 or 1 in binary case.)
 */
class DecisionTreeBuilder;
class DecisionNode;

struct VariableEdge
{
	std::string VariableValue;
	std::string	EdgeLabel;
	DecisionNode *Target;
};

class DecisionNode:public GraphVizNodeDecorator
{
public:
	DecisionNode(std::string node_name);
	virtual ~DecisionNode();
	virtual DecisionNode *Classify(std::vector<std::string> instance)=0;

	void AddChild(VariableEdge *new_child);

	/*
	 * Implement the GraphVizNode Decorator routines to generate graphviz output.
	 */
public:
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();
	virtual std::string GenerateGravizEdgeDecoration(GraphVizNodeDecorator *edge);
	const std::string VariableName;

protected:
	std::vector<VariableEdge *> Children;


};

/*
 * The terminal is an absolute value from the domain of the result value (0 or 1 in binary case)
 */
class TerminalLeaf: public DecisionNode
{
public:
	TerminalLeaf(std::string terminal_value);
	virtual ~TerminalLeaf();
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();
	virtual DecisionNode *Classify(std::vector<std::string> instance);
	const std::string TerminalValue;
};

/*
 * The decision tree node has a classifier specified and built for it by the DecisionTreeBuilder.
 * The Builder determines the algorithm to build the decision tree with.
 */
class DecisionTreeNode: public DecisionNode {
public:
	DecisionTreeNode(std::string variable_name,size_t column);
	virtual ~DecisionTreeNode();

	virtual DecisionNode *Classify(std::vector<std::string> instance);

public:
	/*
	 * This function implemented specifically for CIS 572.
	 */
	std::string GenerateModelFile(size_t depth);

private:
	size_t column_index;

};


/*
 * This is a ID3 tree builder algorithm, it is complex enough to justify its own class and set of functions.
 * The values calculated in entropy and gain will be displayed in graph viz.
 *
 * The plan is to replace the CSVFile with an abstract interface that can return type information on the column,
 *
 *  Then depending on the type, range, and classification, a category decision nodes such as ID3 / C4.5 / TDIDT / CART
 */
class DecisionTreeBuilder
{
public:
	DecisionTreeBuilder(CSVFile *file);
	virtual ~DecisionTreeBuilder();
	bool IsTerminal(std::vector<size_t> class_set, std::string &terminal_value,double gain);
	double Entropy(std::vector<size_t> class_set);
	double Gain(std::vector<size_t> class_set, std::string variable_name);
	std::map<std::string,std::vector<size_t> > SegmentSet(std::vector<size_t> class_set, std::string variable);

public:
	virtual DecisionTreeNode *BuildDecisionTree() = 0;
protected:
	std::string TargetVariable;
	size_t TargetIndex;
	std::map<std::string,std::vector<std::string> > VariableValues;
	std::map<std::string,size_t> IndexMap;
	std::map<size_t,std::string> ReverseIndexMap;
	CSVFile *csv_file;
};

class ID3TreeBuilder:public DecisionTreeBuilder
{
public:
	ID3TreeBuilder(CSVFile *file);
	virtual ~ID3TreeBuilder();
	virtual DecisionTreeNode *BuildDecisionTree();
private:
	DecisionTreeNode *BuildID3Recursive(std::vector<std::string> variables,std::vector<size_t> indexes);
};

/*
 * Not enough time to implement this class yet.
 */
/*
class J48C45:public DecisionTreeBuilder
{
public:
	J48C45(CSVFile *file);
	virtual ~J48C45();
	virtual DecisionTreeNode *BuildDecisionTree();
private:
	DecisionTreeNode *BuildJ48C45Recursive(std::vector<std::string> variables,std::vector<size_t> indexes);
};
*/


#endif /* DECISIONTREENODE_H_ */
