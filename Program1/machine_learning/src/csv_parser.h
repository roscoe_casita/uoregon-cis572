#ifndef CSV_PARSER_H
#define CSV_PARSER_H


#include <string>
#include <sstream>
#include <fstream>
#include <vector>

typedef struct _CSVFile{
	std::vector<std::string> ColumnNames;
	std::vector<std::vector<std::string> > RowsOfColumns;
	//std::vector<std::vector<std::string> > ColumnsOfRows;
} CSVFile;


CSVFile *ReadCSVFile(std::string filename);
void WriteCSVFile(std::string filename, CSVFile *csv);


#endif
