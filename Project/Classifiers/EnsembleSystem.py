from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import AdaBoostClassifier

import numpy as np
import math
import csv
import json
import random
from DataObject import BaseDataContainer
from DataObject import DataObject

class BoostOptions:
    def __init__(self,classifiers_count,max_tree_depth,corruption_percentage,sample_size,min_data):
        self.Classifiers = classifiers_count
        self.binary_y_values = [0.0,1.0]

        self.MaxTreeDepth = max_tree_depth
        self.CorruptionPercentage = corruption_percentage

        self.SampleSize = sample_size
        self.MinData = min_data


    #   These should be removed eventually.
        self.min_estimators = 50
        self.max_extimators = 250

    def GetYValues(self):
        return self.binary_y_values

    def TestYValue(self,Y_value,y_value):
        if Y_value == y_value:
            return True
        return  False

    def CorruptAway(self,Y_value,y_value):
        if Y_value == 1.0:
            y_value = 0.0
        else:
            y_value = 1.0
        return  y_value

    def CorruptTowards(self,Y_value,y_value):

        # this is where the y_value would take on the new gaussian distribution that the _my_Y_value would be
        y_value = Y_value
        return  y_value

    def generateClassifier(self):
        return AdaBoostClassifier(n_estimators=random.randrange(self.min_estimators, self.max_extimators ))



class PredictorNode:
    def __init__(self,boost_options , y_value,matrix_column,tree_depth):
        self.BoostOptions = boost_options
        self.Classifier = self.BoostOptions.generateClassifier()
        self.Column = matrix_column
        self.MyChild = None
        self.MyYValue = y_value
        self.OtherChildren = None
        self.MaxTreeDepth = tree_depth

    '''
    else:
    corrupt_indexes = self.gencorruptIndexNumbers(vector_of_indexes, rate)

    for i in vector_of_indexes:
        if i in corrupt_indexes:
            data = self.base_data[i]
            val_index = random.randint(0, len(corruptableValues) - 1)
            d_val = corruptableValues[val_index]
            data = d_val
            returnValue.append(np.array(data))
        else:
            returnValue.append(np.array(self.base_data[i]))
    '''

    def sub_sample_data(self,data_indexes):

        returnValue = []

        length = len(data_indexes)

        min_value = min(length,self.BoostOptions.SampleSize)

        for i in range(min_value):
            index = data_indexes[random.randint(0,length-1)]
            returnValue.append(index)

        return returnValue


    def build_and_train_on_data(self,other_data_indexes,my_data_indexes,data_container,result_container):

        data_indexes = other_data_indexes
        data_indexes.extend(my_data_indexes)

        data_index_subset = data_indexes
        data_index_subset = self.sub_sample_data(data_index_subset)




        training_indexes = data_index_subset

        training_set = data_container.getDataSubset(training_indexes)
        training_test_set = result_container.getDataSubset(training_indexes)


        internal_node ="Tree internal node: "
        leaf_node ="Tree leaf node reached: "


        self.Classifier.fit(training_set,training_test_set)

        if (self.MaxTreeDepth <= 0 ) or  (len(my_data_indexes) < self.BoostOptions.MinData):
            print(leaf_node+ str(self.MaxTreeDepth) + " Data Prediction node Data with: " + str(len(my_data_indexes)))
            return

        other_other_data_id = []
        other_my_data_id = []
        child_other_data_id = []
        child_my_data_id = []

        other_data = data_container.getDataSubset(other_data_indexes)

        predictions = self.Classifier.predict(other_data)

        for i in range(len(other_data_indexes)):
            prediction = predictions[i]
            index = other_data_indexes[i]

            if self.BoostOptions.TestYValue(self.MyYValue,prediction):
                child_other_data_id.append(index)
            else:
                other_other_data_id.append(index)

        my_data = data_container.getDataSubset(my_data_indexes)

        predictions = self.Classifier.predict(my_data)

        for i in range(len(my_data_indexes)):
            prediction = predictions[i]
            index = my_data_indexes[i]

            if self.BoostOptions.TestYValue(self.MyYValue, prediction):
                child_my_data_id.append(index)
            else:
                other_my_data_id.append(index)


        if len(other_other_data_id)==0 or len(other_my_data_id) == 0 or len(child_other_data_id) ==0 or len(child_my_data_id) == 0:
            print(leaf_node + str(self.MaxTreeDepth) + " Data Prediction node Data with: " + str(len(my_data_indexes)))
            return


        self.MyChild = PredictorNode(self.BoostOptions,self.MyYValue,self.Column,self.MaxTreeDepth-1)
        self.OtherChildren = PredictorNode(self.BoostOptions,self.MyYValue,self.Column,self.MaxTreeDepth-1)

        self.MyChild.build_and_train_on_data(child_other_data_id,child_my_data_id,data_container,result_container)
        self.OtherChildren.build_and_train_on_data(child_other_data_id, child_my_data_id, data_container, result_container)
        return

    def predict(self,data_container, indexes,result_matrix):

        data_set = data_container.getDataSubset(indexes)
        predictions = self.Classifier.predict(data_set)


        if self.MyChild is not None:
            child_indexes = []
            other_indexes = []


            for i in range(len(indexes)):
                p = predictions[i]
                index = indexes[i]

                if self.BoostOptions.TestYValue(self.MyYValue,p):
                    child_indexes.append(index)
                else:
                    other_indexes.append(index)

            if len(child_indexes) > 0:
                self.MyChild.predict(data_container,child_indexes,result_matrix)

            if len(other_indexes) > 0:
                self.OtherChildren.predict(data_container,other_indexes,result_matrix)

        else:
            for i in range(len(indexes)):

                # store the prediction in the result matrix.
                index = indexes[i]
                prediction = predictions[i]
                result_matrix[index][self.Column] = prediction



class PredictorTree:
    def __init__(self,boost_options,y_value,column):
        self.BoostOptions = boost_options
        self.root_node =PredictorNode(boost_options,y_value,column,self.BoostOptions.MaxTreeDepth)
        self.MyYValue = y_value

    def trainPredictorTree(self,data_container,result_container):


        data_indexes = result_container.getFullDataIndex()
        data_set = result_container.complete_set

        other_indexes = []
        my_indexes = []

        for i in data_indexes:
            y = data_set[i]

            if self.BoostOptions.TestYValue(self.MyYValue,y):
                my_indexes.append(i)
            else:
                other_indexes.append(i)

        self.root_node.build_and_train_on_data(other_indexes,my_indexes,data_container,result_container)

        full_data = data_container.complete_set
        full_answers = result_container.complete_set


    def predict(self,data_container,result_matrix):

        index_set = data_container.getFullDataIndex()

        self.root_node.predict(data_container,index_set,result_matrix)


class NeuralNetworkEnsemble:
    def __init__(self,problem_size):
        num_neurons_base = []
        '''
        if neuron_strategy == "log":
            # Log for no posterior
            while problem_size > 1:
                num_neurons_base.append(int(math.ceil(problem_size)))
                problem_size = math.log2(problem_size)
            num_neurons_base.append(1) # Add a final 1 to the end for a single output
        '''
        '''
        if neuron_strategy == "lin":
            num_neurons_base = []

            # incremental for no posterior
            while problem_size > 0:
                num_neurons_base.append(problem_size)
                problem_size -= 1
        '''

        #if neuron_strategy == "const":
        num_neurons_base = [problem_size,1]
        h_layers_base = tuple(num_neurons_base)

        self.predictor = MLPClassifier(hidden_layer_sizes=h_layers_base, random_state=1)
        self.input_width = problem_size

    def train_on_predictions(self,matrix_of_predictions,prediction_container):

        prediction_vector = prediction_container.complete_set
        self.predictor.fit(matrix_of_predictions,prediction_vector)

    def predict(self,matrix_of_predictions):
        return self.predictor.predict(matrix_of_predictions)



class EnsembleTestSystem:
    def __init__(self,  boost_options):

        self.BoostOptions = boost_options

        self.ClassifierForest = []


        i = 0

        # this can now loop over gaussian distributions... its still discrete units.
        for y in self.BoostOptions.GetYValues():
            for c in range(self.BoostOptions.Classifiers):
                pt = PredictorTree(boost_options,y,i)
                self.ClassifierForest.append(pt)
                i = i +1

        self.TotalPredictors = i

        self.Decider = NeuralNetworkEnsemble(i)


    def train_system(self, data_container,result_container):

        # here we need to segment the data, and split it up.

        print("Training Classifiers...")

        for i in range(self.TotalPredictors):
            print("Training classifier " + str(i+1) + " of " + str(self.TotalPredictors))
            self.ClassifierForest[i].trainPredictorTree(data_container,result_container)


        train_matrix = self.tree_predict(data_container)

        print("Training neural network.")
        self.Decider.train_on_predictions(train_matrix, result_container)

    def BuildPredictionMatrix(self, data_container):
        matrix = []
        for i in range(data_container.rows):
            pred = []
            for i in range(self.TotalPredictors):
                classifier = self.ClassifierForest[i]
                pred.append(classifier.MyYValue)

            matrix.append(pred)
        return matrix


    def tree_predict(self,data_container):

        print("Building a forest of y hat predictions.")

        matrix = self.BuildPredictionMatrix(data_container)

        for tree_index in range(self.TotalPredictors):

            self.ClassifierForest[tree_index].predict(data_container,matrix)
        return matrix



    def system_predict(self, data_container):
        print("Making predictions from forest of trees...")

        matrix  = self.tree_predict(data_container)


        print("Predicting final y hat values")

        predictions = self.Decider.predict(matrix)

        return predictions


class FileSupport:
    def __init__(self,fname: str):
        assert isinstance(fname, str)
        self.file_name = fname



    def write_data(self, test_object,predictions):
        with open(self.file_name, 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['RefID', 'IsBadBuy'])
            for x in range(test_object.rows):
                myList = [test_object.refIDs[x], int(predictions[x])]
                writer.writerow(myList)

    def train_test_one_adaboost(self,train_data_object,train_result_object,predict_data_object):
        one_adaboost = AdaBoostClassifier(n_estimators=10000)

        one_adaboost.fit(train_data_object.complete_set,train_result_object.complete_set)

        predictions = one_adaboost.predict(predict_data_object)

        return predictions


def driver(file_out, training_object, test_object, boost_options):


    test_ensemble_system = EnsembleTestSystem(boost_options)

    data_container = BaseDataContainer(training_object.float_data)
    result_container = BaseDataContainer(training_object.target_data)

    test_ensemble_system.train_system(data_container,result_container)
   # test_ensemble_system.train_one_10k_adaboost(training_object)



    data_container = BaseDataContainer(test_object.float_data)

    predictions = test_ensemble_system.system_predict(data_container)
    #test_ensemble_system.predict_10k_adaboost(test_object)

    file_support = FileSupport(file_out)

    file_support.write_data(test_object,predictions)
