#!/usr/bin/python
#
# CIS 472/572 -- Programming Homework #3
#
# Constructed by Roscoe Casita, 03/24/2017
#
#   Multiple sources were amalgamated to construct this project:
#
#       http://machinelearningmastery.com/build-multi-layer-perceptron-neural-network-models-keras/
#       https://keras.io/getting-started/sequential-model-guide/
#


# 3. Import libraries and modules
import numpy as np
np.random.seed(123)  # for reproducibility
 
from keras.models import Sequential

from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from keras.datasets import mnist
from time import sleep

def Load2DData():
    # this is pulled from the sample data
    # 4. Load pre-shuffled MNIST data into train and test sets
    (X_train, y_train), (X_test, y_test) = mnist.load_data()
 
    # 5. Preprocess input data
    X_train = X_train.reshape(X_train.shape[0], 1, 28, 28)
    X_test = X_test.reshape(X_test.shape[0], 1, 28, 28)
    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')
    X_train /= 255
    X_test /= 255

    # 6. Preprocess class labels
    Y_train = np_utils.to_categorical(y_train, 10)
    Y_test = np_utils.to_categorical(y_test, 10)

    return [(X_train,Y_train),(X_test,Y_test)]

def Load1DData():
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    x_train = x_train.reshape(60000, 784)
    x_test = x_test.reshape(10000, 784)
    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    y_train = np_utils.to_categorical(y_train, 10)
    y_test = np_utils.to_categorical(y_test, 10)

    return (x_train, y_train), (x_test, y_test)




# 7. Define model architecture2


def GenModel1():
    model = Sequential()
    # add the 16 hidden sigmoid units.
    model.add(Dense(16,activation='sigmoid',input_shape=(784,)))
    # add the 10 output layer nodes, in this case same activation because it was not specified.
    model.add(Dense(10, activation='sigmoid'))
    model.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])
    model.summary()
    return ["Model1, 16 sigmoid",model]

def GenModel2():
    model = Sequential()
    # add the 128 hidden sigmoid units.
    model.add(Dense(128,activation='sigmoid',input_shape=(784,)))
    # add the 10 output layer nodes, in this case same activation because it was not specified.
    model.add(Dense(10, activation='sigmoid'))
    model.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])
    model.summary()
    return ["Model2, 128 sigmoid",model]

def GenModel3():
    model = Sequential()
    # add 128 hidden rectified linear units.
    model.add(Dense(128,activation='relu',input_shape=(784,)))
    # add the 10 output classification nodes.
    model.add(Dense(10, activation='relu'))
    model.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])
    model.summary()
    return ["Model3, 128 relu",model]

def GenModel4():
    model = Sequential()
    model.add(Dense(128,activation='relu',input_shape=(784,)))
    model.add(Dropout(.5))
    model.add(Dense(10, activation='relu'))
    model.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])
    model.summary()
    return ["Model4, 128 relu, 50% dropout",model]

def GenModel5():
    model = Sequential()
    model.add(Convolution2D(32, 3, 3, activation='relu', input_shape=(1,28,28),dim_ordering='th'))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(.5))
    # activation changes here to softmax, as per the tutorial.
    # activation was specified for the hidden node, otherwise default to the tutorial.
    model.add(Dense(10, activation='softmax'))
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    model.summary()
    return ["Model5, one 2d convolutional relu, one 128 relu layer (50% dropout)", model]

def GenModel6():
    model = Sequential()
    model.add(Convolution2D(32, 3, 3, activation='relu', input_shape=(1,28,28),dim_ordering='th'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(.25))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(.5))
    model.add(Dense(10, activation='softmax'))
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    model.summary()
    return ["Model6, one 2d convolutional relu, one max pooling (25% dropout), one layer 128 relu (50% dropout", model]

def GenModel7():
    model = Sequential()
    model.add(Convolution2D(32, 3, 3, activation='relu', input_shape=(1, 28, 28),dim_ordering='th'))
    model.add(Convolution2D(32, 3, 3, activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(10, activation='softmax'))
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    model.summary()
    return ["Model7, Two 2d convolutional relu, one max pooling (25% dropout), one layer 128 relu (50% dropout)", model]


def EvalModel(model, model_name,x_train,y_train,x_test,y_test,batch_size,epochs):

    history = model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs,verbose=1)

    # 10. Evaluate model on test data
    score = model.evaluate(x_train, y_train, verbose=0)

    string_print = model_name +  ' : train loss: ' + str(score[0]) + "\r\n"
    string_print += model_name + ' : train accuracy: ' + str(score[1]) + "\r\n\r\n"

    score = model.evaluate(x_test, y_test, verbose=0)

    string_print += model_name +  ' : test loss: ' + str(score[0]) + "\r\n"
    string_print += model_name + ' : test accuracy: ' + str(score[1]) + "\r\n\r\n"
    print(string_print)
    return string_print

[(x_train_1d,y_train_1d),(x_test_1d,y_test_1d)] = Load1DData()

[(x_train_2d,y_train_2d),(x_test_2d,y_test_2d)] = Load2DData()

models_1d = []
models_1d.append( GenModel1())
models_1d.append( GenModel2())
models_1d.append( GenModel3())
models_1d.append( GenModel4())

models_2d = []

models_2d.append(GenModel5())
models_2d.append(GenModel6())
models_2d.append(GenModel7())

# 9. Fit model on training data
batch_size = 32
epochs = 30


str_results = []

for [str_name,model] in models_1d:
    str_result = EvalModel(model,str_name,x_train_1d,y_train_1d,x_test_1d,y_test_1d,batch_size,epochs)
    str_results.append(str_result)

for [str_name,model] in models_2d:
    str_result = EvalModel(model,str_name,x_train_2d,y_train_2d,x_test_2d,y_test_2d,batch_size,epochs)
    str_results.append(str_result)


for str_result in str_results:
    print(str_result);

#tensor flow is crashing on this system here?
sleep(1)
import gc; gc.collect()
sleep(1)
