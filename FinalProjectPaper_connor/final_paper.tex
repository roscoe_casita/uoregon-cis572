%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% writeLaTeX Example: Academic Paper Template
%
% Source: http://www.writelatex.com
% 
% Feel free to distribute this example, but please keep the referral
% to writelatex.com
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% How to use writeLaTeX: 
%
% You edit the source code here on the left, and the preview on the
% right shows you the result within a few seconds.
%
% Bookmark this page and share the URL with your co-authors. They can
% edit at the same time!
%
% You can upload figures, bibliographies, custom classes and
% styles using the files menu.
%
% If you're new to LaTeX, the wikibook is a great place to start:
% http://en.wikibooks.org/wiki/LaTeX
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%&pdflatex
\documentclass[twocolumn,showpacs,%
  nofootinbib,aps,superscriptaddress,%
  eqsecnum,prd,notitlepage,showkeys,10pt]{revtex4-1}

\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{dcolumn}
\usepackage{hyperref}
\usepackage{float}


\begin{document}

\title{Augmented Ensemble of AdaBoost Trees vs. Bad Cars}
\author{Connor George and Roscoe Casita}
\affiliation{University of Oregon}

\begin{abstract}
Classification algorithms are used to make predictions about which category a particular example falls into based on related data about that example.  Combining the results of multiple classifiers is often a good way to increase the accuracy of the overall system.  Systems which take advantage of multiple classifiers and combine them to make a better prediction overall are known as ensemble classifiers.  Ensemble classifiers combine and interpret the outputs of their subclassifiers in various ways, but they usually only consider the actual predictions made by each subclassifier.

We hypothesized that adding additional metadata about the predictions that are generated will result in a better overall prediction.  This project focuses on the posterior distribution of each of the interior classifiers.  A classification system was designed which allows for various classifiers to make predictions which are fed into a neural network classifier.  In some cases the posterior distributions were fed into the neural network along with the predictions, and in other cases only the predictions were fed in.  The goal of this project was to determine whether there was a significant difference in accuracy between the two strategies.
\end{abstract}

\maketitle

\section{Introduction}

The primary goal of this project was to determine the extent to which additional metadata about predictions can effect the overall outcome of ensemble systems.  The hypothesis is that adding posterior distribution information to the interior classification algorithm will increase the overall accuracy of the classification system by a statistically significant amount.

If the hypothesis is found to be correct, this would indicate that the performance of classification algorithms could be increased if metadata is passed from the classifiers to the weighing system in addition to the predictions.  This could lead to better overall performance in ensemble systems.  The technique used in this project is fairly generalizable and could prove beneficial for a large variety of ensemble classifiers.

This paper is intended to familiarize the reader with the methods and results of the experiment.  A complete description of all systems, experiments, and results are provided.  After reading this paper, it is intended that the reader could then duplicate this experiment on their own.

\section{Background}

\subsection{AdaBoost}

AdaBoost is a popular boosting algorithm that was used extensively in this project.  Boosting algorithms make use of a large number of weak classifiers in order to create a single strong classifier.  Boosting algorithms are a form of an ensemble algorithms, and so their use in this project resulted in the development of an ensemble system comprised of other ensemble systems.

AdaBoost was one of the first algorithms which made use of boosting techniques to create a better overall classification algorithm.  The reason it was used in this project was due to its similarity to the original plan for the project, and due to it's ensemble structure in nature.  It was also found to be easy to use and provided good baseline accuracy.  Originally it was intended that an algorithm similar to AdaBoost would be implemented and used in this project.  When finalizing the plan for the project, it was determined that AdaBoost was a similar enough algorithm to fit well with the original project vision, and therefore time and effort could be saved by using it rather than developing a new algorithm.

\subsection{Posterior Distribution}

Posterior distribution data is a form of metadata that was as the core of this project's hypothesis.  The hypothesis for this project involved augmenting the prediction trees of an ensemble system with metadata, and the posterior distribution was determined to be the metadata that was most likely to produce interesting results.

The posterior distribution provides data about the accuracy of predictions.  Formally, the posterior distribution can be described as $ \forall_{y\in D(Y)} P(\hat{Y} = y | Y=y) $.  Less formally, the posterior distribution is the accuracy of some particular prediction made by a classifier.  This stands in contrast to the overall accuracy of the system, which does not differentiate between particular predictions, but instead measures the accuracy of the system as a whole.  The posterior distribution was thought to be a good metadata metric to test because it provides more information about a prediction than an accuracy alone.

\subsection{Ensemble Algorithms}

The system developed for this project was an ensemble system which was comprised of several other ensemble systems.  Ensemble algorithms are algorithms that make use of the predictions from multiple other algorithms to generate a more accurate prediction overall.  The algorithm developed for this project was comprised of a neural network and a forest of classifier trees (both discussed later).  The classifier trees were in turn comprised of classification nodes each containing an ensemble algorithm of their own (AdaBoost).  Thus, the entire system is a multilayered ensemble algorithm comprised of several layers of other ensemble algorithms.


\section{Methods \& Experiments}

\subsection{Dataset}

The dataset for this project was taken from a competition hosted on www.kaggle.com.  The competition was called ``Don't Get Kicked!".  Training and test datasets of cars were provided with this competition, and the goal was to predict whether a car would be a good or bad purchase.  Car dealerships often purchase used cars at auctions, but each purchase carries a risk.  Some cars have serious issues that would prevent the dealership from being able to resell the car to customers (cars like this are known as ``kicks").  Kicked cars carry a huge cost to the dealership due to many factors, such as transportation costs, repair work that is wasted on a vehicle that will never be sold, and the up-front purchase cost of the car itself.

For obvious reasons dealerships would want to avoid purchasing a kicked car as much as possible.  This competition challenged participants to predict whether a car would be a good or bad purchase based on many other data features about the cars.  The datasets that were provided included numeric data, categorical data, and text data.  A few examples of these data include mileage information as numeric data, transmission type as categorical data, and make/model/submodel information as text data (in addition to many other features).  To simplify the process of testing the project algorithm, all text data was discarded, and categorical data was converted to float data and normalized (categories were stored as integers).

The data included null values for some of the columns.  Two strategies for handling null values were considered.  The first strategy was to simply set all null values equal to zero (for float data), and to set null values in categorical columns to some new category of their own.  The other strategy that was considered was to set each null value to the ``typical" values of the column (average value for float columns and most common value for categorical).  Both options had merits, and the decision of which strategy is most effective is an interesting question on its own.  Based on limited testing, setting null values to ``typical" values for float data and considering null values for categorical data to be a category of their own was the better of the two strategies.  This was based on the resulting accuracy, and was therefore chosen as the default strategy for this project.


\subsection{Classifier Trees}

The algorithm that was developed for this project can be broken down into two main parts.  The first part is a layer of classifier trees, and the second part consisted of a neural network which accepted input from the classifier trees (described more later). The classifier trees are more complicated and interesting, and will be discussed first.

A classifier tree is a tree-like data structure in which each of the nodes consists of a single classifier algorithm.  For this project, the AdaBoost algorithm was used because the techniques used in the classifier tree were similar to boosted algorithms, and AdaBoost was a good choice for a classic boosted algorithm.  However, it is important to note that any classification algorithm could be used in place of AdaBoost to test the hypothesis, or any combination of algorithms.  The effects of including the posterior distribution could vary depending on the algorithm used.  The choice of which algorithm to use is and interesting question that deserves an entire project of its own, but due to time constraints the AdaBoost algorithm was chosen.  The code was written in a generic way that would allow for easy modification if a different algorithm was desired.

The classifier tree was built on the fly as the nodes were trained.  The process for building the tree involved creating a root classifier node and training a randomized oversampled subset of the entire dataset on it.  Next, the training dataset was passed into the node again and predictions were made for the training set.  The classifier node created a child node for each of the possible prediction categories.  After making predictions on the dataset, the data was divided based on the prediction that was made for each example.  Each subset was then passed to the appropriate child node and the process was repeated.

This process continues until a predefined maximum depth of the tree was reached.  A max depth of five was used for this project.  Since trees grow in size exponentially as the maximum depth increases, and training each node could take a long time, a relatively small maximum depth was necessary in order to avoid extreme amounts of required computing time.  Additionally, a minimum dataset size was defined such that nodes would only be created if there was a large enough dataset size.  This was done in order to account for the fact that there were far more 0 values in the dataset than there were 1 values.  It naturally led to the data being split unevenly when new nodes were created, and creating an entire node to train on a very small sample size is undesirable.

If each node in the tree had the exact same parameters and were trained on the same datasets, they all would have made the same predictions for each example.  This would have resulted in a very inefficient system for making predictions because new information would not be gained at each level of the tree.  To counter this, the number of estimators in each AdaBoost algorithm was randomized in order to provide variety at each of the nodes.  Using a combination of different classifiers would have provided even more variety, but would also have greatly increased the scope of the project.

To make a prediction using this prediction tree, the test data is passed to the root node.  Predictions are made on it, and the examples with a particular prediction are passed to the corresponding child node.  The child node then makes a prediction and the process is repeated.  In this way, a particular example from the dataset follows a path through the tree beginning at the root node and ending at one of the leaf nodes.  The leaf nodes then make a final prediction which is the prediction that is output to the neural network.  In this way, multiple classifiers can make predictions on the same data, and the path through the tree results in different posterior distributions at each leaf node.  For example, a test row that ends up at the leftmost leaf node is very likely to be a 0 row in reality because all classifiers that predicted it agreed that it was a 0.  Leaf nodes in the center of the tree represent lower confidence predictions because there must have been disagreement among the predictors for an example to end up in the center.

Each leaf node then calculates its posterior distribution.  This is the key feature of the classifier tree that lies at the heart of the project.  The posterior distribution consists of the probabilities of each possible classification given that the actual class is that classification.  More formally, the posterior distribution consists of the following probabilities.

$$P(\hat{y} = y | Y = y)$$
$$P(\hat{y} = \bar{y} | Y = \bar{y})$$

These probabilities represent the probability that a particular classifier is able to accurately classify a particular value of $y$, or the confidence that a particular prediction is accurate.  This is not the same as the overall accuracy of the system in which the probability of any prediction being correct has a particular value.  Many other learning algorithms such as decision trees and logistic regression are able to make predictions and provide a confidence as well.  The key difference between those systems and this system is that the probabilities are related to particular values of $\hat{y}$ in this system.  Other systems do not typically make use of posterior distributions.  For this experiment, the predictions as well as the posterior distributions were passed to the neural network.  Additionally, a version was run where only the predictions were passed to the neural network in order to create a control for the experiment.

\begin{figure}[H]
	\includegraphics[scale=.2]{system_architecture}
	\caption{\label{fig:your-figure}Diagram of system without augmented predictions}
\end{figure}


\subsection{Neural Network}

Many classifier trees were created for the entire ensemble system, and each produced a single set of output values.  These output values are then passed into a neural network.  The neural network makes the final prediction of the entire ensemble system, and the prediction it makes is based on the input values from all previous classifiers.  In this way, the neural network never directly interacts with the dataset.  Rather, it combines the classifier data in a way that weighs each classifier appropriately.

The neural network is trained after the classification trees are trained.  The classification trees finish training and then the training data is passed through them so that predictions can be made.  These predictions then serve as the basis for training the neural network.  In this way, the training of the entire system is performed on the training set, but is not done all at the same time.

The neural network uses a constant node layer scheme in which the number of nodes in the first layer is equal to the number of trees in the ensemble system.  Then, the output from the first layer is passed into one final neuron which makes up the entirety of the output layer.  This neuron's final prediction represents the prediction of the overall system.  Several other schemes were considered, but were discarded due to producing invalid results most of the time.  One such system was a logarithmic strategy in which each layer had a number of neurons equal to the log value of the previous layer's number of neurons, until only a single node was needed for the output layer.  A linear strategy was tested as well.  In this strategy, the number of neurons in the first layer was equal to the number of classifier trees, and at each subsequent layer the number of neurons was decremented until a final output layer with one neuron was created.  Both of these strategies were found to be inefficient and inaccurate, and were abandoned in favor of the more accurate and more efficient constant strategy.



An overview makes the entire system clearer.  First, training data is passed to the classifier trees and they train on that data by first training the root node and then predicting, splitting, and passing the data to the proper node.  Next, the neural network is trained by passing the training data through the forest of classifier trees to get their prediction values.  The neural network is trained on these prediction values.  Finally, the entire system can be used to make predictions on the test set by passing the test set through the forest of classifier trees, then passing the predictions generated by the trees to the neural network, then finally outputting the final prediction from the neural network.

\begin{figure}[H]
	\includegraphics[scale=.2]{augmented_system_architecture}
	\caption{\label{fig:your-figure}Diagram of system with augmented predictions}
\end{figure}


\section{Results}

The hypothesis for this experiment was rejected.  It was found that augmenting the ensemble system with posterior distribution metadata did not result in a significant improvement in accuracy for the overall system.  However, these results are not conclusive due to the presence of possible confounding factors in the system.  There are several possible reasons why the system was not able to perform better, and a lack of meaning in the posterior distribution is only one of many potential explanations.  Some results were produced that indicate that further exploration into the problem may demonstrate that posterior distribution information can be meaningful in the right setting.  However, it is impossible to say with certainty without further examination.

It was discovered that the system was able to produce better results than a single AdaBoost algorithm trained with 100,000 predictors.  This indicates that some meaningful pattern may exist in the posterior distribution information that could lead to better predictions overall.  However, since the fundamental structure of the system differs greatly from an AdaBoost classifier, more exploration into this topic is necessary.  The fact that it was able to outperform an AdaBoost though shows promise for future work.

The posterior distribution was also able to consistently alter the results, though typically by a minimal amount.  It also did not consistently increase or decrease the performance of the system.  Sometimes the accuracy was increased, and sometimes it was decreased.  This indicates that some effect is present from the posterior distribution, but it is difficult to tell whether the differences are meaningful or if they are simply random variations that arise from the fact that different data is being classified.

\begin{figure}[H]
\includegraphics[scale=.45]{DataGraph}
\caption{\label{fig:your-figure}Graph displaying the variation between the posterior distribution augmented data and the non-augmented data}
\end{figure}

Finally, it was also found that large feature sets provided more accurate predictions than small feature sets by a significant amount.  This was expected, but is worth noting because it indicates that additional information about the problem could have had a greater effect regarding the posterior distributions.  If there were more features that were provided to the system, posterior distributions could have been more meaningful and could have produced better results.


\section{Conclusion \& Future Work}

The hypothesis for this project was rejected due to a lack of correlation between the use of posterior data in the system and the overall accuracy of the system.  Many steps could be taken to further explore the extent to which metadata effects the prediction accuracy of the system.  Testing the system with a different dataset would provide insight as to whether certain forms of data are more effected by posterior distribution information than others.  Testing with another dataset would help to control for unique qualities of the dataset that was used.  Similarly, using a classifier other than AdaBoost would help to control for unique qualities of the AdaBoost algorithm.  Since the AdaBoost algorithm was not inherently necessary for the experiment it is possible that another algorithm or combination of algorithms could produce better results.

Another area of exploration is to more thoroughly test the system with different parameters.  This ensemble system was comprised of many different subsystems, and each subsystem had a variety of parameters that could be tuned.  This lead to a very large domain of possible system configurations, and due to time constraints this domain of parameters could not be fully explored.  However, it is possible that with the right combination of parameters posterior data could be more meaningful in making predictions.  One change in particular that could lead to interesting results would be to change the depth and breadth of the neural network.  Since the neural network that accepted augmented data was inputting more total data, it had a relative increase in the size of its first hidden layer.  Future work would include tuning neural network dimensions for both augmented and non-augmented datasets.

Another area of exploration that has been considered would be to create a system which is tuned to only classify a single particular category from the set of categories $Y$.  This can be accomplished by isolating all training values of a particular category and then intentionally corrupting the $Y$ values of a specific percentage of that subset.  This specific strategy would be used in a classifier tree that was tuned to identify a subset of the specific $\hat{Y}$ values.  This strategy would need to be repeated multiple times for every possible value in the domain of $Y$.  Thus, each potential category would have a forest of predictors associated with it, and each of these forests would be connected to the neural network.  This is inspired from the techniques used in AdaBoost, and it is thought that it may be possible to produce accurate classifications while using less computational power than an equivalent AdaBoost.

This project served as a good learning experience regarding the process of exploring a topic using machine learning techniques, developing a system to test that topic, and interpreting the results of the system.  This project was particularly good for demonstrating how machine learning classifiers can be connected to produce more interesting results than they could as standalone predictors.  The project also demonstrated the importance of selecting the correct data to train a system with.  Since the heart of the project was to determine the particular worth of certain kinds of data, it provided clear evidence that some data are more necessary for making accurate predictions than others.  Determining the correct data to use is key to any successful machine learning task, and is a topic that requires extensive thought before beginning any project that involves machine learning.  Finally, the project provided both experimentors with good practice regarding the overall nature of machine learning problems.  This practice will prove useful for any future projects that the experimentors work with.

\clearpage
\section{references}

\begin{itemize}
\item Brodersen, K. H., Ong, C. S., Stephan, K. E., \& Buhmann, J. M. (2010, August). The balanced accuracy and its posterior distribution. In Pattern recognition (ICPR), 2010 20th international conference on (pp. 3121-3124). IEEE.
\item Bengio, Y., Le Roux, N., Vincent, P., Delalleau, O., \& Marcotte, P. (1997). AdaBoosting neural networks. In Neural Computation.
\item Schwenk, H., \& Bengio, Y. (2000). Boosting neural networks. Neural Computation, 12(8), 1869-1887.
\item Dietterich, T. G. (2000). An experimental comparison of three methods for constructing ensembles of decision trees: Bagging, boosting, and randomization. Machine learning, 40(2), 139-157.
\item Tanner, M. A., \& Wong, W. H. (1987). The calculation of posterior distributions by data augmentation. Journal of the American statistical Association, 82(398), 528-540.
\item Wang, G., \& Li, P. (2010, August). Dynamic Adaboost ensemble extreme learning machine. In Advanced Computer Theory and Engineering (ICACTE), 2010 3rd International Conference on (Vol. 3, pp. V3-54). IEEE.
\item Zhu, J., Zou, H., Rosset, S., \& Hastie, T. (2009). Multi-class adaboost. Statistics and its Interface, 2(3), 349-360.
\end{itemize}


\end{document}